﻿using UnityEngine;


[RequireComponent(typeof(playerMotor))]
public class playerController : MonoBehaviour {
    [SerializeField] //makes it visible in inspector with it being protected
    private float speed = 5f;
    [SerializeField]
    private float lookSensitivity = 3f;

    private playerMotor motor;

    private void Start()
    {
        motor = GetComponent<playerMotor>();
    }
    private void Update()
    {
        //Calculate movement velocity as a 3D vector
        float xMove = Input.GetAxisRaw("Horizontal");
        float zMove = Input.GetAxisRaw("Vertical");

        //Final Movement Vector
        Vector3 moveHorizontal = transform.right * xMove;
        Vector3 moveVertical = transform.forward * zMove;

        //Apply Movement
        Vector3 _velocity = (moveHorizontal + moveVertical).normalized * speed;

        motor.Move(_velocity);

        //Calculate rotation as 3d Vector: Turning Around
        float yRot = Input.GetAxisRaw("Mouse X");


        Vector3 _rotation = new Vector3(0f, yRot, 0f) * lookSensitivity;

        //Apply Rotation

        motor.Rotate(_rotation);

        //Calculate Camera rotation as 3d Vector: Turning Around
        float xRot = Input.GetAxisRaw("Mouse Y");


        Vector3 cameraRotation = new Vector3 (xRot, 0f, 0f) * lookSensitivity;

        //Apply Rotation

        motor.RotateCamera(cameraRotation);

    }
}
