﻿using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class playerMotor : MonoBehaviour {
    [SerializeField]
    private Camera cam;
    private Vector3 velocity = Vector3.zero;
    private Rigidbody rb;
    private Vector3 rotation = Vector3.zero;
    private Vector3 cameraRotaion = Vector3.zero;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    //Gets a movement vector
    public void Move(Vector3 _Velocity)
    {
        velocity = _Velocity;
    }
    private void FixedUpdate()
    {
        performMovement();
        performRotation();

    }
    //Perform movement based on velocity variable
    void performMovement()
    {
        if (velocity != Vector3.zero)
        {
            rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
        }
    }
    public void Rotate(Vector3 _rotation)
    {
        rotation = _rotation;
    }
    void performRotation()
    {
        rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));
        if (cam != null)
        {
            cam.transform.Rotate(-cameraRotaion);
        }
    }
    public void RotateCamera(Vector3 _cameraRotation)
    {
        cameraRotaion = _cameraRotation;
    }
}
